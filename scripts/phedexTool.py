#!/usr/bin/env python
import logging
import argparse
import json
from phedexOperationsManager import PhEdExOperationManager

log_choices = [ 'ERROR', 'WARNING', 'INFO', 'DEBUG' ]
log = logging.getLogger( 'phedexTool' )

def setupLogging( args ):
    #setup logging
    format = '%(levelname)s (%(name)s): %(message)s'
    date = '%F %H:%M:%S'
    logging.basicConfig( level=logging._levelNames[ args.debug ],
                         format=format,
                         datefmt=date )
    log.setLevel(logging._levelNames[ args.debug ])
    formatter = logging.Formatter( format )
    hdlr = logging.FileHandler('phedexToolLog.txt')
    hdlr.setFormatter(formatter)
    log.addHandler(hdlr)

def main():
    description='Perform PhEdEx storageConsistencyChecks and handle results'

    parser = argparse.ArgumentParser()
    parser.add_argument('--debug',
                        metavar='LEVEL',
                        default='ERROR',
                        choices=log_choices,
                        help='Set the debug level.'\
                         'Allowed values: ' + ', '.join( log_choices ) +
                         ' [default: %(default)s]' )
    parser.add_argument('-s', '--summary-json',
        help='Use results from previous run')
    parser.add_argument('-n', '--nagios',
        help='Produce nagios compliant output')
    PhEdExOperationManager.add_command_line_options(parser)
    args = parser.parse_args()

    if args.nagios:
        args.debug = "ERROR"
    # setup logging
    setupLogging( args )

    manager = PhEdExOperationManager(args)
    if args.summary_json:
        with open( args.summary_json, "r" ) as json_file:
            summary = json.load(args.summary_json)
    else:
        manager.performConsistencyCheck()
    if args.customLFN:
        manager.compare_custom_lfn_list()
    if args.nagios:
        print manager.checker.nagiosStatus

if __name__ == '__main__':
    main()
