# Phedex Tool
A collection of maintanance tools for PhEdEx operations at T2_DE_RWTH


## Preparing the tools
Before using the phedex_tools you should be logged in as the phedex user,
source phedex (e.g. `source /home/phedex/sw/slc6_amd64_gcc493/cms/PHEDEX/4.2.1/etc/profile.d/env.sh`)
and source the phedex_tools (`source phedex_tools/set_env.sh`),

## Performing consistency checks

Performing a first consistency check:
```
phedexTool.py --debug INFO ../phedex_tools/configs/consistencyCheck_RWTH.cfg
```
This command will:

**Check consistency**
* Create a list of all LFNs in the requested subdirs of /store/ using one of two methods
  * Using the mount point for /pnfs/ as defined in the "storepath" config variable
  * Using a call to the chimera-dump.py tool on the dCache headnode when  the `--chimeraDump` option is added (faster and more reliable)
* Call the official PhEdEx StorageConsistencyCheck tool with the previously created LFN list as the input
* Parse the output from the StorageConsistencyCheck command in lists of orphans and missing files.

**Orphan handling**
* Match existing orphans to log entries for failed delete requests (optional with `--skip-log-match`).
* Delete orphans if requested (`--delete-orphans`). A log match is required if the `--delete-without-log` is not used.

**Missing file handling**
* The StorageConsistencyCheck tool provides only datasets and numbers of missing files for the set and the phedexTool finds the corresponding files
  by gathering the expected files for a dataset via the PhEdEx web API and comparing the output to the existing files on the SE.
* A summary and a GGUS ticket draft for invalidation is created for the found missing files
* The /pnfs/ mount might hang from time to time which might lead to "permission denied" errors. It can be restarted using `/etc/init.d/autofs restart`

### Caching
Each time consuming step saves intermediate results in text files which might be provided to phedexTool.py in order
to skip some steps:
* `--chimeraDump` existing chimera dumps
* `--existingLFN` existing list off all LFNs
* `--output` Output from a previous run of storageConsistencyCheck

### Comparing to a custom LFN list
Sometimes custom list of some LFNs are provided (e.g. by PhEdEx operators) in order to
be checked at your site. The StorageConsistencyCheck however needs a full LFN list in order to
produce a reasonable output. The `--customLFN` otpion may be used to provide a list of LFNs
and check which of the are orphaned or missing

### Nagios output
Consistency checks may also be used as a nagios test using the `--nagios` option.
In this case only one nagios stauts line will be printed.

## Checking error logs
Sometimes it is necessary to query PhEdEx errors and parse their output from the web API
The transferErrorTool.py allows to construct simple queries and use the xml output for further analysis.
