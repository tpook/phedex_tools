#!/usr/bin/env python
from PhEdExAPI import *
import argparse

log_choices = [ 'ERROR', 'WARNING', 'INFO', 'DEBUG' ]
log = logging.getLogger( 'phedexTool' )

def setupLogging( args ):
    #setup logging
    format = '%(levelname)s (%(name)s): %(message)s'
    date = '%F %H:%M:%S'
    logging.basicConfig( level=logging._levelNames[ args.debug ],
                         format=format,
                         datefmt=date )
    log.setLevel(logging._levelNames[ args.debug ])
    formatter = logging.Formatter( format )

def main():
    parser = argparse.ArgumentParser( description = "Filter logs")
    parser.add_argument("--from", help='name of the source node, could be multiple')
    parser.add_argument("--to", help='name of the destination node, could be multiple')
    parser.add_argument("--lfn", help='logical file name')
    parser.add_argument("--dataset", help='logical file name')
    parser.add_argument("--block", help='logical file name')
    parser.add_argument("--error", help='Error to filter for')
    parser.add_argument('--debug',
                        metavar='LEVEL',
                        default='ERROR',
                        choices=log_choices,
                        help='Set the debug level.'\
                         'Allowed values: ' + ', '.join( log_choices ) +
                         ' [default: %(default)s]' )

    query_keys = ("from", "to", "lfn", "dataset", "block")
    args = parser.parse_args()
    query_dict = { key: val for key,val in args.__dict__.items() if val and key in query_keys}

    xmldoc = post_request( 'errorlog',
                       **query_dict
                     )
    file_list =  xmldoc.getElementsByTagName('file')
    for file_doc in file_list:
        log = file_doc.getElementsByTagName('detail_log')[0]
        if args.error and args.error in log.toxml():
            print file_doc.attributes['name'].value

        #~ print file_doc.toxml()

if __name__ == '__main__':
    main()
