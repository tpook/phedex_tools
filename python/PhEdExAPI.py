##
#
# PhEdEx.py: Python interface for PhEdEx web API, see
# https://cmsweb.cern.ch/phedex/datasvc/doc/
#
# author: Tobias Pook
#

from __future__ import print_function
from xml.dom import minidom
import urllib2
import logging

PHEDEX_BASE_URL = "https://cmsweb.cern.ch/phedex/datasvc/xml/"

phedex_api_options = {}

log = logging.getLogger('PHEDEXAPI')

subscription_options = {
  'dataset'        : 'dataset name (wildcards)',
  'block'          : 'block name (wildcards)',
  'node'           : 'node name (wildcards) or id. Can be multiple, but cannot mix names and ids in the same request',
  'se'             : 'storage element',
  'create_since'   : 'timestamp. only subscriptions created after.*',
  'request'        : 'request number(s) which created the subscription.',
  'custodial'      : 'y or n to filter custodial/non subscriptions.\n default is null (either)',
  'group'          : 'group name filter',
  'priority'       : 'priority, one of "low", "normal" and "high"',
  'move'           : 'y (move) or n (replica)',
  'suspended'      : 'y or n, default is either',
  'collapse'       : 'y or n. default y. If y, do not show block level subscriptions of a dataset if it was subscribed at dataset level.',
  'percent_min'    : 'only subscriptions that are this complete will be shown.',
  'percent_max'    : 'only subscriptions less than this complete will be shown. N.B. percent_min may be greater than percent_max, to exclude subscriptions that lie between the two numerical limits'
}
phedex_api_options['subscriptions'] = subscription_options

blockreplicas_options = {
 'block'          : 'block name, can be multiple (*)',
 'dataset'        : 'dataset name, can be multiple (*)',
 'node'           : 'node name, can be multiple (*)',
 'se'             : 'storage element name, can be multiple (*)',
 'update_since'   : 'unix timestamp, only return replicas whose record was updated since this time',
 'create_since'   : 'unix timestamp, only return replicas whose record was created since this time. When no "dataset", "block" or "node" are given, create_since is default to 24 hours ago',
 'complete'       : '  y or n, whether or not to require complete or incomplete blocks. Open blocks cannot be complete.  Default is to return either.',
 'dist_complete'  : 'y or n, "distributed complete".  If y, then returns only block replicas for which at least one node has' \
                    'all files in the block.  If n, then returns block'\
                    'replicas for which no node has all the files in the'\
                    'block.  Open blocks cannot be dist_complete.  Default is'\
                    'to return either kind of block replica.',
 'subscribed'     : 'y or n, filter for subscription. default is to return either.',
 'custodial'      : 'y or n. filter for custodial responsibility.  default is to return either.',
 'group'        : '  group name.  default is to return replicas for any group.',
 'show_dataset' : '  y or n, default n. If y, show dataset information with the blocks; if n, only show blocks',
}
phedex_api_options['blockreplicas'] = blockreplicas_options

filereplicas_options = {
 'block'          : 'block name, with * wildcards, can be multiple (*).  required when no lfn is specified. Block names must follow the syntax /X/Y/Z, i.e. have three / Anything else is rejected.',
 'dataset'        : 'dataset name. Syntax: /X/Y/Z, all three / obligatory. Wildcads are allowed.',
 'node'           : 'node name, can be multiple (*)',
 'se'             : 'storage element name, can be multiple (*)',
 'update_since'   : 'unix timestamp, only return replicas updated since this time',
 'create_since'   : 'unix timestamp, only return replicas created since this time',
 'complete'       : 'y or n. if y, return only file replicas from complete block' \
                    'replicas.  if n only return file replicas from incomplete block'\
                    'replicas.  default is to return either.',
 'dist_complete'  : 'y or n.  if y, return only file replicas from blocks'\
                    'where all file replicas are available at some node. if'\
                    'n, return only file replicas from blocks which have'\
                    'file replicas not available at any node.  default is'\
                    'to return either.',
 'subscribed'     : 'y or n, filter for subscription. default is to return either.',
 'custodial'      : 'y or n. filter for custodial responsibility.  default is' \
                    'to return either.',
 'group'          :  'group name.  default is to return replicas for any group.'
}
phedex_api_options['filereplicas'] = filereplicas_options

group_usage = {
  'node'          : 'node name, could be multiple',
  'se'            : 'storage element name, could be multiple',
  'group'         : 'group name, could be multiple'
}
phedex_api_options['groupusage'] = filereplicas_options

errorlog_options = {
 'from'           :  'name of the source node, could be multiple',
 'to'             :  'name of the destination node, could be multiple',
 'block'          :  'block name',
 'dataset'        :  'dataset name',
 'lfn'            :  'logical file name'
}
phedex_api_options['errorlog'] = errorlog_options

def post_request( command, ph_instance = 'prod', dump = False, **kwargs ):
    ''' Generalized function to post a command to PhEdEx web API.
        Options can be passed as keyword arguments. Dump option may be used to save raw xml.'''

    # check if the command is available
    if not command in phedex_api_options:
        log.error( "%s is not available in PheEdExAPI" % command)
        return minidom.parse( "" )

    # construct query from kwargs
    query_list = []
    for key in kwargs:
        if key in ["ph_instance", "dump" ]: continue
        if key in phedex_api_options[ command ]:
            query_list.append( '%s=%s' % ( key, kwargs[ key ] ) )
        else:
            log.warning( "Option %s not available for PhEdEx API command %s." % ( key, command ) )
    query = ""
    if len( query_list) > 0 : query = "?" + "&".join( query_list )

    # create url and post request
    url = PHEDEX_BASE_URL + ph_instance + "/" + command + query
    #print( url )
    raw_xml = urllib2.urlopen( url )
    if dump:
        with open( "%_dump.xml" % command, "w") as outxml:
            outstring =""
            for node in datasetlist:
                outxml.write( node.toprettyxml() )

    # parse returned xml with minidom
    try:
        xmldoc = minidom.parse( raw_xml )
    except:
        log.error( "ERROR: Can not parse retured xml for command %s" % command )
        log.error( "with query: %s" % query )

    try:
        errors = xmldoc.getElementsByTagName('error')
        for error in errors:
            log.error( "Web API returned an error:" )
            log.error( error.toprettyxml() )
    except:
        pass
    return xmldoc
