#!/usr/bin/env python
# Author Tobias Pook 2016 tpook@cern.ch
import os
import sys
import gzip
import logging
import glob
import subprocess
import argparse
import fnmatch
import subprocess
from utils import MyConfigParser

log_choices = [ 'ERROR', 'WARNING', 'INFO', 'DEBUG' ]
log = logging.getLogger( 'consistencyCheckHelper' )

class ConsistencyCheck():

    def __init__(self, config=None):

        # List of strings for checkConsistencyCheck output
        self.rawOuts = []

        # list of subdirs of /store/ that should be considered
        self.storeSubdirs = ["mc"]

        # Results
        self.nLFNs = 0
        self.nDirs = 0
        # Total number of files listed in TMDB for SE
        self.nTotalTMDB = 0

        self.nMissing = 0
        self.nOrphaned = 0
        self.nUnwatchedDirs = 0

        # list of paths to orphaned files by /store/ subdir
        self.orphanedFiles = {}
        self.missingDirs = {}
        # dictonary of list of dictionaries with comparision for
        # directories with differences between SE / TMDB
        self.dirSummaries = {}
        self.allDirSummaries = {}
        # Dictionary subdir -> nFiles. Keys are subdirs of /store/
        # where TMDB expects files, values are number of expected files
        self.allStoreDirs = {}
        if not config is None:
            self.readConfig( config )

    #helperfunction to receive orphaned files as flat list
    @property
    def orphanedFileList( self ):
        allFiles = []
        for subd in self.orphanedFiles:
            allFiles += self.orphanedFiles[ subd ]
        return allFiles

    def readConfig(self, configPath ):
        self.config = MyConfigParser()
        self.config.read( configPath )
        self.basepath = self.config.get( "GENERAL" , "phedexbase")
        self.utilspath = self.config.get( "GENERAL" , "phedexUtils")
        self.dbpath = self.config.get( "GENERAL" , "dbinstance")
        self.node = self.config.get( "GENERAL" , "node")
        self.SEname = self.config.get( "GENERAL" , "SEname")
        self.storepath = self.config.get( "GENERAL" , "storepath")
        self.subdirs = self.config.get( "GENERAL" , "subdirs").split()
        self.chimeraDumpCommand = self.config.get( "CHIMERA" , "chimera_dump_command")
        self.chimeraDumpPath = self.config.get( "CHIMERA" , "chimera_dump_path")
        if not self.dbpath.startswith("/"):
            self.dbpath = os.path.join( self.basepath , self.dbpath)
        if not self.utilspath.startswith("/"):
            self.utilspath = os.path.join( self.basepath , self.utilspath)

    @classmethod
    def pfn2lfn( cls, pfn ):
        if not "/store/" in pfn:
            return False
        return "/store/" + pfn.split("/store/")[-1]

    def createLFNlist( self, chimeraDump=False, existingChimeraDump=None ):

        allLFNs = []
        if chimeraDump:
            if not existingChimeraDump:
                self.chimeraDumpPath = self.chimeraDumpPath.replace(".gz","")
                self.chimeraDumpCommand = "ssh phedex@grid-dcache \"python /data/DMWMMON/DCache/tools/chimeraDump/chimera-dump.py -s /pnfs/physik.rwth-aachen.de/cms/ -c consistency_dump -a -g -o /opt/exp_soft/rwth/nobackup/phedex/cms.consistency.storage.dump\""
                log.info(self.chimeraDumpCommand)
                subprocess.call(self.chimeraDumpCommand,shell=True)
            else:
                self.chimeraDumpPath = existingChimeraDump
            log.info(self.chimeraDumpPath)
            f = gzip.open(self.chimeraDumpPath + ".gz", 'r')
            lines = f.read().split("\n")
            f.close()
            for line in lines:
                try:
                    pfn = line.split()[1]
                except:
                    log.error("Parse Error for line:")
                    log.error(line)
                    continue
                if not pfn.endswith(".root"):
                    continue
                lfn = self.pfn2lfn(pfn)
                if not any( [lfn.startswith("/store/" + subd) for subd in self.subdirs ] ):
                    continue
                allLFNs.append( lfn )
        else:
            for subd in self.subdirs:
                fullpath = os.path.join( self.storepath, subd )
                for root, dirnames, filenames in os.walk( fullpath ):
                    for filename in fnmatch.filter(filenames, '*.root'):
                        allLFNs.append(os.path.join(root, filename))
        allLFNs = list(set(allLFNs))
        #~ print allLFNs
        log.info("Found %d lfns in watched subdirs" % len(allLFNs))
        f = gzip.open('LFNlist.txt.gz', 'wb')
        f.write( "\n".join( allLFNs ) )
        f.close()
        self.lfnFile = 'LFNlist.txt.gz'

    def performConsistencyCheck( self ,
                                 createLFNlist = True,
                                 chimeraDump = False,
                                 existingChimeraDump=None):

        if createLFNlist:
            log.info( "Creating new LFN list (may take some time)")
            self.createLFNlist(chimeraDump=chimeraDump,
                               existingChimeraDump=existingChimeraDump)
        cmd = " ".join( [os.path.join( self.utilspath, "StorageConsistencyCheck" ),
                        "--db",
                        self.dbpath,
                        "--lfnlist",
                        self.lfnFile,
                        "--node",
                        self.node ])
        log.info( "Running consistency check with command:")
        log.info( cmd )
        p = subprocess.Popen(cmd,stdout=subprocess.PIPE, shell=True)
        try:
            (out,err) = p.communicate()
        except RuntimeError:
            (out,err) = ("","")
            log.warning("StorageConsistencyCheck timeout")

        self.rawOuts = out.split("\n")

    def parseConsistencyOutput(self):

        # list of paths to orphaned files by store subdir
        orphanedFiles = {}
        # dictonary of list of dictionaries with comparision for directories with differences
        dirSummaries = {}
        allDirSummaries = {}

        # variables for loop
        addOrphaned = False
        addDirSummary = False
        for i,line in enumerate(self.rawOuts):
            if i < 1: continue
            # Try to get number of LFNs and dirs from output
            if i == 1 and line.startswith("Got"):
                splitline = line.split()
                try:
                    try:
                        self.nLFNs = int( splitline[1] )
                    except ValueError:
                        log.error("Can not parse number of LFNs %s" % splitline[1])
                    try:
                        self.nDirs = int( splitline[4] )
                    except ValueError:
                        log.error("Can not parse number of dirs %s" % splitline[4])
                except IndexError:
                     log.error("Unexpected string for nLFN and dir parsing" )
                     log.error( line )
            if line.startswith("Orphaned file summary:"):
                addOrphaned = True
                continue

            # add files to list of orphaned files while we are in orphaned section
            if addOrphaned:
                if line.strip().endswith(".root"):
                    checkedStoreDir = line.split("/")[2]
                    if checkedStoreDir in self.orphanedFiles:
                        self.orphanedFiles[ checkedStoreDir ].append( line )
                    else:
                        self.orphanedFiles.update( { checkedStoreDir : [ line ] } )
                else: addOrphaned=False

            # parse summary for directories with differences
            if line.startswith("Directory summary:"):
                addDirSummary = True
                continue
            if "SE/TMDB" in line or "Finished..." in line: continue
            if addDirSummary:
                splitline = line.split()
                if len(splitline) < 1: continue
                try:
                    nSE = int( splitline[0].split("/")[0] )
                    nTMDB = int( splitline[0].split("/")[1] )
                    self.nTotalTMDB += nTMDB
                except ValueError:
                    log.error("Can not parse number on SE/TMDB" )
                    log.error( line )
                    nSE = -1
                    nTMDB = -1
                try:
                    checkedDir = line.split(":")[1].strip()
                    checkedStoreDir = checkedDir.split("/")[2]
                    # save number of expected files by subdir of store
                    if nTMDB > 0 :
                        if checkedStoreDir in self.allStoreDirs:
                            self.allStoreDirs[ checkedStoreDir ] += nTMDB
                        else:
                            self.allStoreDirs.update( { checkedStoreDir : nTMDB} )
                except IndexError:
                    log.error( " Can not parse checked dir in summary")
                    log.error( line )
                    checkedDir = "ERROR"
                    checkedStoreDir = "ERROR"

                dictSum = {"dir" : checkedDir, "nSE": nSE, "nTMDB" : nTMDB }

                if checkedStoreDir in self.allDirSummaries:
                    self.allDirSummaries[ checkedStoreDir ].append( dictSum )
                else:
                    self.allDirSummaries.update({checkedStoreDir: [ dictSum ] })
                # check if this subdir of /store/ was checked and skip summary otherwise
                # as it contains no meaningful comparision
                if checkedStoreDir in self.subdirs:
                    if checkedStoreDir in self.dirSummaries:
                        self.dirSummaries[ checkedStoreDir ].append( dictSum )
                    else:
                        self.dirSummaries.update({checkedStoreDir: [ dictSum ] })

    def readResultFromFile( self, infilePath ):
        ofile = gzip.open('consistencyCheckDump.txt.gz', 'r')
        self.rawOuts = [ line.replace('\n','') for line in ofile.readlines() ]
        ofile.close()

    def dumpRawOut( self ):
        # gzip does not support with statement in python 2.6
        out_file = gzip.open('consistencyCheckDump.txt.gz', 'wb')
        out_file.write( "\n".join( self.rawOuts ) )
        out_file.close()

    @property
    def nagiosStatus( self ):
        # get thresholds from config
        # TODO remove repeated code
        try:
            missingWarning = int ( self.config.get( "NAGIOS" , "missingWarning") )
        except ValueError:
            log.error( "Config parsing to int failed for NAGIOS missingWarning" )

        try:
            missingCritical = int ( self.config.get( "NAGIOS" , "missingCritical") )
        except ValueError:
            log.error( "Config parsing to int failed for NAGIOS missingCritical" )

        try:
            orphanedWarning = int ( self.config.get( "NAGIOS" , "orphanedWarning") )
        except ValueError:
            log.error( "Config parsing to int failed for NAGIOS orphanedWarning" )

        try:
            orphanedCritical = int ( self.config.get( "NAGIOS" , "orphanedCritical") )
        except ValueError:
            log.error( "Config parsing to int failed for NAGIOS orphanedCritical" )


        testname = self.config.get( "NAGIOS" , "testname")
        perfData = "OrphanedFiles=%d;%d;%d;;" % ( self.nOrphaned, orphanedWarning, orphanedCritical)
        perfData += " MissingFiles=%d;%d;%d;;" % ( self.nOrphaned, missingWarning, missingCritical)
        state = 3

        if self.nMissing == 0 and self.nOrphaned == 0: state = 0
        elif self.nMissing > missingCritical:            state = 2
        elif self.nOrphaned > orphanedCritical:          state = 2
        elif self.nMissing > missingWarning:             state = 1
        elif self.nOrphaned > orphanedWarning:           state = 1

        if state == 0:
            report = [" Storage consistent with TMDB"]
        elif state > 0 and state <3:
            report = ["nMissing: %d nOrphaned: %d" % ( self.nMissing, self.nOrphaned )]
            report += ["Details:"]
            report += ["Datasets with missing files:"]
            #~ for subd in self.missingDirs:
                #~ report += self.missingDirs[subd]
            #~ report += ["Orpaned Files:"]
            #~ for subd in self.orphanedFiles:
                #~ report += self.orphanedFiles[subd]
        if state == 3 and self.nTotalTMDB == 0:
            report += ["No info from TMDB"]

        nagiosStatusString = '{state} {testname} {perfdata} - {message}'.format( state = str(state),
                                                                                 testname = testname,
                                                                                 perfdata = perfData,
                                                                                 message = '\\n'.join(report) )
        return nagiosStatusString


    def nOrphanedCheck( self, subdir, path ):
        if not subdir in self.orphanedFiles: return 0
        nOrphaned = 0
        for ofile in self.orphanedFiles[ subdir ]:
            if path in ofile: nOrphaned +=1
        return nOrphaned

    def analyseResult( self ):
        self.nMissing = 0
        self.nOrphaned = 0
        self.nUnwatchedDirs = 0

        log.info( "Total number of Files expected from TMDB: %d" % self.nTotalTMDB )
        log.info( "Subdirs of /store/ monitored by TMDB:" )
        log.info( '{0:<20}   {1:<7}   {2:<7}   {3:<8}  {4:<6}   {5:<7}  {6:<6}'.format("store subdir",
                                                                     "TMDB",
                                                                     "SE",
                                                                     "Orphaned",
                                                                     "%",
                                                                     "Missing",
                                                                     "%") )
        for subd in self.allStoreDirs:
            sumSubTMDB = 0
            sumSubSE = 0
            for info in self.allDirSummaries[subd]:
                sumSubTMDB += info["nTMDB"]
                sumSubSE += info["nSE"]
                #~ if info["nTMDB"] > 0:
                    #~ self.nMissing += (info["nSE"] - info["nTMDB"])
                # check and add directory to dict of directories with missing files
                if sumSubTMDB > ( sumSubSE - self.nOrphanedCheck( subd, info["dir"] ) ):
                    nDirMissing = sumSubSE - self.nOrphanedCheck( subd, info["dir"] )
                    if subd in self.missingDirs:
                        self.missingDirs[subd].append( info["dir"] )
                    else:
                        self.missingDirs.update( { subd: [ info["dir"] ] } )
            try:
                nSubOrphaned = len(self.orphanedFiles[subd])
            except KeyError:
                nSubOrphaned = 0
            if sumSubSE > 0:
                nSubOrphanedRatio = 100.* nSubOrphaned / sumSubSE
            else:
                nSubOrphanedRatio = 0.
            nSubMissing = sumSubTMDB - ( sumSubSE - nSubOrphaned )
            # add subfolder results to to total count
            self.nMissing += nSubMissing
            self.nOrphaned += nSubOrphaned

            log.info( '{0:<20}   {1:<7}   {2:<7}   {3:<8}  {4:<6}   {5:<7}  {6:<6}'.format(subd,
                                                                            sumSubTMDB,
                                                                            sumSubSE,
                                                                            nSubOrphaned,
                                                                            "%.1f"%nSubOrphanedRatio,
                                                                            nSubMissing,
                                                                            "%.1f"%(100. * nSubMissing / sumSubTMDB)
                                                                            ) )
        for searchD in self.subdirs:
            if not searchD in self.allStoreDirs:
                log.warning("Your config requests to check /store/%s which is not monitored by TMDB" %searchD)
                self.nUnwatchedDirs += 1

def setupLogging( args ):
    #setup logging
    format = '%(levelname)s (%(name)s): %(message)s'
    date = '%F %H:%M:%S'
    logging.basicConfig( filename="consistencyCheck.log", level=logging._levelNames[ args.debug ], format=format, datefmt=date )
    log.setLevel(logging._levelNames[ args.debug ])
    formatter = logging.Formatter( format )

def main():
    #~ "consistencyCheck011015.txt"
    # parse command line arguments
    parser = argparse.ArgumentParser(description='Perform and analyse PhEdEx storageConsistencyChecks')
    parser.add_argument('config', type=str, help='A valid config file')
    parser.add_argument('--output', type=str, help='Path to a text file containing the output from a previously run of storageConsistencyCheck')
    parser.add_argument( '--debug', metavar='LEVEL', default='WARNING', choices=log_choices,
                       help='Set the debug level. Allowed values: ' + ', '.join( log_choices ) + ' [default: %(default)s]' )
    args = parser.parse_args()

    # setup logging
    setupLogging( args )

    checker = ConsistencyCheck( args.config )
    if args.output:
        checker.readResultFromFile( args.output )
    else:
        checker.performConsistencyCheck( )
    checker.parseConsistencyOutput()
    checker.dumpRawOut()
    checker.analyseResult()
    print checker.nagiosStatus
if __name__ == '__main__':
    main()
