#!/usr/bin/env python
# Author Tobias Pook 2016 tpook@cern.ch
import os,sys
import glob
import gzip
import logging
import argparse
import time
from datetime import datetime, timedelta
import json
import fnmatch

from consistencyCheckHelper import ConsistencyCheck
from utils import MyConfigParser, LogInfo
from PhEdExAPI import *

# setup logging
log = logging.getLogger(__name__)

class PhEdExOperationManager( object ):
    ''' Main class to perform different checks and operations related to
        PhEdEx operations: consistency checks, cleanup of orphans,
        determination of lost files and matching to relevant log infos.
    '''
    def __init__( self, options ):
        self.options = options
        # create Consistency Check instance
        self.checker = ConsistencyCheck(self.options.config)

        # types of log to consider
        self.log_types = ['remove', 'download']
        self.log_infos = {}

        # read the config file
        self.readConfig()

        if not options.skip_log_match:
            self.readLogs()

        # Dict to save results in machine readable json
        self.summary_json = {}

        # cache for phedex requests
        self.phedex_request_cache = {}

    def readConfig(self):
        ''' read config and safe infos in options object'''
        log.debug('Reading config %s' % self.options.config)
        configobj = MyConfigParser()
        configobj.read(self.options.config)
        self.options.srm_prefix = configobj.get('GENERAL', 'srm-prefix')
        storepath = configobj.get('GENERAL', 'storepath')
        self.options.storepath = storepath.replace('/store','')
        self.options.phedexbase = configobj.get('GENERAL', 'phedexbase')
        self.options.logdir = configobj.get('GENERAL', 'logdir')
        for log_type in self.log_types:
            logname = configobj.get('GENERAL', '%s-log' % log_type)
            setattr(self.options, '%s_logname' % log_type, logname)

    def readLogs(self):
        ''' Read all log files and store the related LogInfos in lists
            sorted by logtype in logs_infos member attribute dict
         '''
        # convert earliest_log to datetime object
        try:
            self.options.earliest_log = datetime.strptime(
                self.options.earliest_log, '%Y-%m-%d %H:%M:%S')
        except:
            log.error('Unable to parse earliest-log option, exiting')
            sys.exit(1)

        for log_type in self.log_types:
            # get archived logs
            log.debug('Reading log files')
            logname = getattr(self.options, '%s_logname' % log_type)
            pattern = '%s.*.gz' % logname
            if not self.options.logdir.startswith('/'):
                full_log_path = os.path.join(self.options.phedexbase,
                                             self.options.logdir)
            else:
                full_log_path = self.options.logdir

            log.debug(pattern)
            log.debug(os.path.join(full_log_path, pattern))
            loglist = glob.glob(os.path.join(full_log_path,pattern))
            log.debug(loglist)
            # add latest log
            if os.path.exists(os.path.join(full_log_path, logname)):
                loglist.append(os.path.join(full_log_path, logname))
            log.debug('Start parsing %s logs ' % logname)
            log.debug('Found %d files' % len(loglist))
            for log_path in loglist:
                log_list = self.readSingleLog(log_path, log_type)
                if not log_list:
                    continue
                if not log_type in self.log_infos:
                    self.log_infos[log_type] = []
                self.log_infos[log_type] += log_list
            # make sure list of log_infos is sorted by time of occurence
            if log_type in self.log_infos:
                self.log_infos[log_type].sort(key=lambda r: r.date,
                                                reverse = True)
        import sys

    def readSingleLog(self, log_path, log_type):
        ''' Read a single log file and return list of LogInfo objects'''
        loginfo_list = []
        log.debug('Reading log ' + log_path)
        if log_path.endswith('.gz'):
            logfile = gzip.open(log_path, 'rb')
            lines = logfile.readlines()
            logfile.close()
        else:
            with open(log_path, 'r') as logfile:
                lines = logfile.readlines()
        #some etries span multiple lines.
        full_entry = ''
        for i,line in enumerate(lines):
            line = line.replace('\n', '')
            if line.startswith('20'):
                full_entry = line
            else:
                full_entry += '\n' + line
                continue
            if LogInfo.is_info_line( full_entry, log_type,
                                      self.options.srm_prefix ):
                loginfo = LogInfo(log_type,
                                  self.options.srm_prefix,
                                  self.options.storepath)
                try:
                    loginfo.parse_log_line(full_entry)
                except ValueError as e:
                    log.error('Error parsing line %d:' % i)
                    log.error(line)
                    log.error(e)
                if loginfo.date < self.options.earliest_log:
                    continue
                loginfo_list.append(loginfo)

        return loginfo_list

    def performConsistencyCheck(self):
        ''' Perform a PhEdEx consistency check and match output to
            relevant log entries.
        '''
        if self.options.output:
            log.info('Reading info from previous consistency check')
            self.checker.readResultFromFile( self.options.output )
        else:
            log.info('Performing consistencyCheck')
            newLFN = True
            if self.options.existingLFN is not None:
                self.checker.lfnFile = self.options.existingLFN
                newLFN = False
            self.checker.performConsistencyCheck( newLFN,
                                                  self.options.chimeraDump,
                                                  self.options.existingChimeraDump)
            # dump raw output for repeated runs
            self.checker.dumpRawOut()
        log.info('Finish consistencyCheck')
        self.checker.parseConsistencyOutput()
        self.checker.analyseResult()
        # Handle orphaned files ( In LFN list but not in TMDB)
        if self.checker.orphanedFileList:
            log.debug(len(self.checker.orphanedFileList))
            orphan_summary = self.handleOrphans()
            self.writeSummary('orphans', orphan_summary)

        # Handle missing files ( In TMDB but not in LFN list)
        if any([sampledir_list for subdir, sampledir_list in
                self.checker.missingDirs.items()]):
            missing_summary = self.handleMissing()
            date_tag = datetime.today().strftime('%Y-%m-%d')
            self.missing_file_name = 'missing_%s' % date_tag
            self.writeSummary(self.missing_file_name + "summary",
                              missing_summary)
            self.writeSummary(self.missing_file_name,
                              "\n".join(self.missingFiles))
            if self.options.show_ticket_draft:
                self.show_ticket_draft()
        self.writeSummaryJson()

    def handleOrphans(self):
        ''' Match orphaned files to logs, delete if requested
            and create a summary info file (text and json)
        '''
        nDeleted = 0
        self.summary_json['orphans'] = []
        summary = 'Summary of orphaned file handling\n'
        n_orphans = len(self.checker.orphanedFileList)
        summary += 'Found %d orphans \n\n' % n_orphans

        log.debug('Handling orphans')
        log.debug(len(self.checker.orphanedFileList))

        for orphan in self.checker.orphanedFileList:
            info_dict = { 'lfn': orphan,
                          'deleted' : 0,
                          'log_entries' : {}
                        }
            summary += orphan

            summary += '\n'
            remove_log_found = False
            if not self.options.skip_log_match:
                related_logs = self.matchLogs( orphan )
                for log_type, log_infos in related_logs.items():
                    log_lines = []
                    summary += 'File LFN occurs %d times' % len(log_infos)
                    summary += orphan
                    summary += 'in %s logs:\n'% log_type
                    for log_info in log_infos:
                        summary += log_info.full_line
                        log_lines.append(log_info.full_line)
                    info_dict['log_entries'][log_type] = log_lines
                if 'remove' in info_dict['log_entries']:
                    if len(info_dict['log_entries']['remove']) > 0:
                        remove_log_found = True
            self.summary_json['orphans'].append(info_dict)

            do_delete = remove_log_found
            do_delete = do_delete or self.options.delete_without_log
            do_delete = do_delete and self.options.delete_orphans
            if do_delete:
                orphan_path = self.options.storepath + orphan
                log.debug("Checking existence of %s" % orphan_path)
                if os.path.exists( orphan_path ):
                    log.debug('Deleting orphan: %s' % orphan)
                    try:
                        os.remove( orphan_path )
                    except:
                        log.debug('File deletion failed')
                    # check again if deletion was sucessfull
                    if not os.path.exists( orphan_path ):
                        summary += '[DELETED]'
                        info_dict[ 'deleted' ] = 1
            else:
                if not remove_log_found and not self.options.delete_without_log:
                    log.debug("Skipping file due to missing matched log: %s" % orphan)
                else:
                    log.debug("Not do delete")
        return summary

    def handleMissing(self):
        log.info( 'Missing files found in %d store subfolder' %
            len(self.checker.missingDirs) )
        self.missing_files = []
        self.summary_json['missing'] = []
        summary = ''
        #~ summary += '(subscribed in TMDB but not in storage)\n'
        for subdir, lfn_list in self.checker.missingDirs.items():
            info = '\n%d datasets with missing files in %s' % (
                len(lfn_list), subdir)
            log.info('### %s ###' % info )
            for lfn in lfn_list:
                s_chunk, s_dict = self._handleMissingSample(lfn, subdir)
                summary += s_chunk + '\n'
                self.summary_json['missing'].append(s_dict)
        return summary

    @property
    def missingFiles(self):
        ''' Helper function for flat list of missing files '''
        mis = []
        if not 'missing' in self.summary_json:
            return mis
        for sdict in self.summary_json['missing']:
            mis += [ fdict['lfn'] for fdict in sdict['files'] ]
        return mis


    def _get_dataset_name(self, lfn, subdir):
        '''Construct datasetname for PhEdEx web api from lfn '''
        splitdir = lfn.split('/')
        if subdir == 'results':
            dataset_name = '/' + splitdir[4] + '/' + splitdir[5]
            dataset_name += '/' + splitdir[7]
        elif subdir == 'data':
            dataset_name = '/' + splitdir[4] + '/' + splitdir[3]
            dataset_name += '*-' + splitdir[6] + '/' + splitdir[5]
        else:
            dataset_name = '/' + splitdir[4] + '/' + splitdir[3]
            dataset_name += '-' + splitdir[6] + '/' + splitdir[5]
        return dataset_name

    def _handleMissingSample(self, lfn, subdir):
        '''Handle missing files for a single sample. This includes
           queries to the phedex web api to gather the expected file and
           matching with existing files on SE. All expected files
           without match are further mathed to log entries and included
           in summaries
        '''
        summary_dict = {'sample' : lfn, 'files' : []}
        summary_chunk = ''
        if lfn.startswith('/'):
            lfn_chunk = lfn[1:]
        else:
            lfn_chunk = lfn
        fullpath = os.path.join( self.options.storepath,
                                 lfn_chunk )

        #get list of all root files for this dataset
        sample_root_files = self._walk_files( fullpath,
                                              '*.root',
                                              self.options.storepath)

        log.debug('Sending request to PhEdEx Web API')
        dataset_name = self._get_dataset_name(lfn, subdir)
        rkey = 'filereplicas' + self.checker.node + dataset_name
        if not rkey in self.phedex_request_cache:
            self.phedex_request_cache[ rkey ] = post_request( 'filereplicas',
                                                    node = self.checker.node,
                                                    dataset= dataset_name
                                                    )
        xmldoc = self.phedex_request_cache[ rkey ]
        file_replica_list =  xmldoc.getElementsByTagName('file')
        if subdir == "data":
            log.debug("dataset_name: " + dataset_name)
        for file_replica in file_replica_list:
            replica_name = file_replica.attributes['name'].value

            if not lfn in replica_name:
                continue
            if replica_name in self.checker.orphanedFileList:
                continue
            if subdir == "data":
                log.debug("replica_name: " + replica_name)
            #~ print replica_name
            if not replica_name in sample_root_files:
                file_infos = {'lfn' : replica_name,
                              'log_entries' : {} }
                summary_chunk += replica_name + '\n'
                self.missing_files.append(replica_name)
                #~ print replica_name
                if not self.options.skip_log_match:
                    related_logs = self.matchLogs( replica_name )
                    for log_type, log_infos in related_logs.items():
                        log_lines = []
                        #~ summary_chunk += 'File LFN occurs %d times' %(
                            #~ len(log_info_list))
                        #~ summary_chunk += 'in %s logs:\n' % log_type
                        for log_info in log_infos:
                            summary_chunk += log_info.full_line
                            log_lines.append(log_info.full_line)
                        file_infos['log_entries'][log_type] = log_lines
                summary_dict['files'].append( file_infos )
        prefix = 'Found %d missing files for sample' % (
            len(summary_dict['files']) )
        prefix += dataset_name + '### \n\n'
        summary_chunk = prefix + summary_chunk
        return summary_chunk, summary_dict

    def n_missing_check(self, lfn, subdir):
        lfn_dicts = self.checker.dirSummaries[subdir]
        return []

    def _walk_files(self, path, extension='*.root', basepath=''):
        ''' Walk through all files in path and return them as list.
            basepath is removed from filenames if given
        '''
        file_list = []
        for root, dirnames, filenames in os.walk( path ):
            for filename in fnmatch.filter(filenames, '*.root'):
                #add all root files to list and remove basepath
                file_name = '/' + os.path.join(root, filename)
                if basepath:
                    file_name = file_name.replace( basepath, '' )
                file_list.append( file_name )
        return file_list

    def matchLogs(self, lfn):
        ''' Match all log infos for a given lfn path '''
        related_logs = {}
        for log_type, log_info_list in self.log_infos.items():
            for log_info in log_info_list:
                if lfn in log_info.lfn:
                    if not log_type in related_logs:
                        related_logs[ log_type ] = []
                    related_logs[ log_type ].append( log_info )
        return related_logs

    def show_ticket_draft(self):
        draft= 'Dear CMS Datatransfer Operators,\n'
        draft+= 'We recently lost some files at our local storage.\n'
        draft+= 'Please invalidate the files listed in the attached '
        draft+= 'file %s for site %s\n' % (self.missing_file_name,
                                           self.checker.node)
        draft+= '\n \n'
        draft+= 'thanks'
        print draft

    def compare_custom_lfn_list(self):
        ''' compare consistency check output with custom lfn list'''
        log.info('Start comparing consistency check output with custom lfn list')
        log.debug(self.options.customLFN)
        with open(self.options.customLFN, "r") as lfnfile:
            lfn_list = lfnfile.read().split("\n")
        orphans = []
        missing = []
        unknown = []
        for lfn in lfn_list:
            if lfn in self.checker.orphanedFileList:
                orphans.append(lfn)
            elif lfn in self.missingFiles:
                missing.append(lfn)
            else:
                unknown.append(lfn)
        log.info("## %d orphans:" % len( orphans))
        log.debug(orphans)
        log.info("## %d missing:" % len( missing))
        log.debug(missing)
        log.info("## %d unknown:" % len( unknown))
        log.debug(unknown)
        return missing, orphans, unknown

    def writeSummary(self, name, summary):
        with open( name + '.txt', 'w' ) as txt_file:
            txt_file.write(summary)

    def writeSummaryJson(self):
        with open( 'phedex_summary.json', 'w' ) as json_file:
            json.dump( self.summary_json, json_file )

    @classmethod
    def add_command_line_options(cls, parser):
        ''' Add needed command line options to existing parser '''
        parser.add_argument('config',
            help='Config file, see configs folder for an example')
        parser.add_argument('--existingLFN', metavar='LFN_LIST',
            help='Path to a text file containing the LFN list used as input for'\
            'the constistency check. A new one will be created if not set.')
        parser.add_argument('--existingChimeraDump', metavar='DUMP', default = None,
            help='Path to a chimera raw dump as produced '\
            'by Fred Stobers chimera-list tool')
        parser.add_argument('--chimeraDump', action="store_true",
            help='Perform a chimeraDump operation using chimera_dump_command in config ')
        parser.add_argument('--customLFN', metavar='LFN_LIST',
            help='Path to a text file containing the LFN list which is compared'\
            'to the consistency check output ')
        parser.add_argument('--output',
            help='Path to a text file containing the output from a'\
                 'previously run of storageConsistencyCheck')
        parser.add_argument('--delete-orphans', action='store_true',
            help='Delete all detected orphan files.')
        parser.add_argument('--show-ticket-draft', action='store_true',
            help='Shows a draft for a GGUS ticket regarding the'\
            'invalidation of missing files')
        # get todays date minus 2 month as default and convert to string
        default_earliest = datetime.today()-timedelta(days=60)
        default_earliest = default_earliest.strftime('%Y-%m-%d %H:%M:%S')
        parser.add_argument('--earliest-log', default= default_earliest,
            help='Earliest date where logs are considered. '\
                'Too many logs may slowdown matching significantly. '\
                'Default considers last 60 days: %(default)s '\
                'Input format is same as in default string ')
        parser.add_argument('--delete-without-log', action='store_true',
            help='Delete detected orphan even if no '\
                  'remove entry is found in logs')
        parser.add_argument('--skip-log-match', action='store_true',
            help='Do not match results to log files'\
            'It is recommended to run first with out the option '\
            'and check the summary')


if __name__ == '__main__':
    main()
