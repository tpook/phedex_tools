#!/usr/bin/env python
# Author Tobias Pook 2016 tpook@cern.ch
import ConfigParser
from datetime import datetime

class MyConfigParser(ConfigParser.SafeConfigParser):
    """Can get options() without defaults
    """
    def optionsNoDefault(self, section):
        """Return a list of option names for the given section name."""
        try:
            opts = self._sections[section].copy()
        except KeyError:
            raise NoSectionError(section)
        if '__name__' in opts:
            del opts['__name__']
        return opts.keys()
    def optionxform(self, optionstr):
        return optionstr

class LogInfo(object):
    ''' Container class for info chunck in log file.
        Usually contains info from one logged operation
    '''
    def __init__( self,
              log_type,
              srm_prefix,
              storepath):
        # String with the lfn
        self.lfn = None
        self.date = None
        self.full_line = None
        self.log_type = log_type
        self.srm_prefix = srm_prefix
        self.storepath = storepath

    def parse_log_line(self, line):
        ''' Parse a single log line '''
        # parse date and time of log entry
        splitline = line.split( ":" )
        date_str =  splitline[:3]
        # some commands are not separated by another : but a space
        date_str[-1] = date_str[-1].split(" ")[0]
        date_str = ":".join(date_str)
        self.date = datetime.strptime( date_str, '%Y-%m-%d %H:%M:%S')
        # parse file paths
        splitline = line.split( self.srm_prefix )
        if len( splitline ) > 1:
            splitline = splitline[1].split(self.storepath)[1]
            splitline = splitline.split('.root')[0]
            splitline = '/store/' + splitline + '.root'
            splitline = splitline.replace('//','/')
            self.lfn = splitline
        self.full_line = line

    @classmethod
    def is_info_line(cls, line, log_type, srm_prefix ):
        ''' Check if a single log line contains infos for the type of log info'''
        trigger_found = False
        if srm_prefix in line:
            trigger_found = True

        return trigger_found
